package edu.intermediate.sesi3

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import edu.intermediate.sesi3.data.News
import edu.intermediate.sesi3.data.newsList
import edu.intermediate.sesi3.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private var _layout: ActivityMainBinding? = null

    private val layout: ActivityMainBinding
        get() = _layout ?: throw IllegalStateException("The activity has been destroyed")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        _layout = binding
        setContentView(binding.root)

        layout.root.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            adapter = NewsAdapter(populateList()) { news ->
                val intent = Intent(context, DetailActivity::class.java).apply {
                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    putExtra(DetailActivity.NEWS, news)
                }
                startActivity(intent)
            }
        }
    }

    private fun populateList(): ArrayList<News> = newsList

    override fun onDestroy() {
        super.onDestroy()
        _layout = null
    }
}
