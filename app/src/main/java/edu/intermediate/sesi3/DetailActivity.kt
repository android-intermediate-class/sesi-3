package edu.intermediate.sesi3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.squareup.picasso.Picasso
import edu.intermediate.sesi3.data.News
import edu.intermediate.sesi3.databinding.ActivityDetailBinding
import java.lang.IllegalStateException
import java.text.SimpleDateFormat
import java.util.Locale

class DetailActivity : AppCompatActivity() {

    private var _layout: ActivityDetailBinding? = null

    private val layout: ActivityDetailBinding
        get() = _layout ?: throw IllegalStateException("The activity has been destroyed")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityDetailBinding.inflate(layoutInflater)
        _layout = binding
        setContentView(binding.root)

        val news = intent.extras?.getParcelable(NEWS) ?: News()

        val inputDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
        val outputDateFormat = SimpleDateFormat("dd MMM yyyy HH:mm", Locale("id", "ID"))
        val date = inputDateFormat.parse(news.publishedDate)

        Picasso.get()
            .load(news.imageUrl)
            .error(R.drawable.broken_image)
            .into(layout.ivNews)
        layout.tvTitle.text = news.title
        layout.tvDate.text = outputDateFormat.format(date)
        layout.tvAuthor.text = news.author
        layout.tvDesc.text = news.desc
    }

    override fun onDestroy() {
        super.onDestroy()
        _layout = null
    }

    companion object {
        const val NEWS = "news"
    }
}